import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

import productPage from "../pages/productPage";
import cartPage from "../pages/cartPage";
import homePage from "../pages/homePage";
import searchPage from "../pages/searchPage";
import productData from "../../fixtures/product.json";

Given("the user is on the home page", () => {
  homePage.accessStore();
});

When("user search the product", () => {
  homePage.type_searchInput(productData.productId);
  homePage.click_searchInputButton();
  searchPage.validate_searchResult();
});

And("add the product to cart", () => {
  searchPage.click_searchResult();
  productPage.add_productToCart();
  cartPage.click_goToCartButton();
});

Then("the product must be added to cart", () => {
  cartPage.check_itemAddedToCart();
});
