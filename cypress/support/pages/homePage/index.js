/// <reference types="Cypress" />

const homePageElements = require("./elements").ELEMENTS;

class homePage {
  accessStore() {
    cy.visit("/");
  }

  type_searchInput(productId) {
    cy.get(homePageElements.searchInput).type(productId);
  }

  click_searchInputButton() {
    cy.get(homePageElements.searchInputButton).click({ force: true });
  }
}

export default new homePage();
